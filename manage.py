#!/usr/bin/env python3
import os
import sys

PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
path = os.path.join(PROJECT_ROOT, 'apps')  # path to your working directory.

if path not in sys.path:
    sys.path.append(path)

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "postal.settings")

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
