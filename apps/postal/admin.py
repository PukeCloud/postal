from solo.admin import SingletonModelAdmin
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import User, Font, Postcard, SiteConfigModel


admin.site.register(User, UserAdmin)
admin.site.register(Font, admin.ModelAdmin)
admin.site.register(Postcard, admin.ModelAdmin)
admin.site.register(SiteConfigModel, SingletonModelAdmin)
