from django.views.generic import TemplateView, ListView, DetailView, View
from django.views.generic.edit import FormMixin
from django.http.response import HttpResponseBadRequest, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import login
from django.contrib.auth.views import LogoutView
from .forms import LoginForm, RegisterForm, PostcardFormSet, SendPostcardForm
from .models import Postcard, Font


class AuthRequiredMixin(View):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated():
            return HttpResponseRedirect(reverse('auth'))
        return super().dispatch(request, *args, **kwargs)


class AuthView(TemplateView, FormMixin):
    template_name = 'auth.html'
    login_form = LoginForm
    register_form = RegisterForm

    def get_success_url(self):
        return reverse('index')

    def get_context_data(self, **kwargs):
        kwargs.update({
            'form': {
                'login': self.login_form,
                'register': self.register_form
            }
        })
        return super().get_context_data(**kwargs)

    def process_login_form(self):
        form = self.login_form(**self.get_form_kwargs())
        if form.is_valid():
            login(self.request, form.get_user())
            return self.form_valid(form)
        else:
            self.login_form = form
            return self.form_invalid(form)

    def process_register_form(self):
        form = self.register_form(**self.get_form_kwargs())
        if form.is_valid():
            form.instance.is_active = True
            user = form.save()
            login(self.request, user)
            return self.form_valid(form)
        else:
            self.register_form = form
            return self.form_invalid(form)

    def post(self, request, *args, **kwargs):
        data = request.POST.copy()
        if 'form' not in data:
            return HttpResponseBadRequest()
        if data['form'] == 'Login':
            return self.process_login_form()
        elif data['form'] == 'Register':
            return self.process_register_form()
        else:
            return HttpResponseBadRequest()


class IndexView(ListView, AuthRequiredMixin):
    model = Postcard
    template_name = 'index.html'
    ordering = '-created_at'


class CreatePostcardView(TemplateView, AuthRequiredMixin):
    model = Postcard
    template_name = 'create.html'
    formset_class = PostcardFormSet
    formset = None
    formset_queryset = Postcard.objects.none()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'formset': self.formset if self.formset else self.formset_class(queryset=self.formset_queryset),
            'fonts': Font.objects.all()
        })
        return context

    def post(self, request, *args, **kwargs):
        self.formset = self.formset_class(data=request.POST, files=request.FILES)
        if self.formset.is_valid():
            for form in self.formset:
                form.instance.author = self.request.user
            self.formset.save()
            return HttpResponseRedirect(reverse('index'))
        else:
            return super().get(request, *args, **kwargs)


class DetailPostcardView(DetailView, FormMixin, AuthRequiredMixin):
    model = Postcard
    pk_url_kwarg = 'card_id'
    template_name = 'detail.html'
    form = SendPostcardForm

    def get_success_url(self):
        return reverse('index')

    def get_context_data(self, **kwargs):
        kwargs.update({
            'form': self.form
        })
        return super().get_context_data(**kwargs)

    def post(self, request, *args, **kwargs):
        form = self.form(**self.get_form_kwargs())
        if form.is_valid():
            self.object = self.get_object()
            self.object.send(
                sender=request.user,
                recipient=form.cleaned_data['recipient'],
                message=form.cleaned_data['message']
            )
            return self.form_valid(form)
        else:
            self.form = form
            return self.form_invalid(form)


class CustomLogoutView(LogoutView):
    def get_next_page(self):
        return reverse('auth')
