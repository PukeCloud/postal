def run():
    import os
    import sys
    from credentials import SETTINGS

    path = os.path.join(SETTINGS['BASE_DIR'], 'apps')
    if path not in sys.path:
        sys.path.append(path)

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "postal.settings")
