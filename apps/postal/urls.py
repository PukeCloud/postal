from django.conf.urls import url, include
from django.contrib import admin
from . import views


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^auth/$', views.AuthView.as_view(), name='auth'),
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^create/$', views.CreatePostcardView.as_view(), name='create'),
    url(r'^(?P<card_id>\d+)$', views.DetailPostcardView.as_view(), name='detail'),
    url(r'^logout/$', views.CustomLogoutView.as_view(), name='logout')
]
