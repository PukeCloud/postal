import os
import json

SETTINGS = {
    'BASE_DIR': os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))),
    'ENV': os.environ.get('ENV', 'vagrant')
}

filename = os.path.join(SETTINGS['BASE_DIR'], 'deploy', 'settings', SETTINGS['ENV'], 'credentials.json')

with open(filename, 'r') as file:
    SETTINGS.update(json.load(file))
