DEBUG = True

EMAIL_BACKEND = 'post_office.EmailBackend'
POST_OFFICE = {
    'BACKENDS': {
        'default': 'django.core.mail.backends.filebased.EmailBackend'
    }
}
EMAIL_FILE_PATH = '/tmp/app-messages'
