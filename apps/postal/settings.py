import os
from .credentials import SETTINGS


if SETTINGS['ENV'] == 'vagrant':
    from .local_settings.vagrant import *


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = SETTINGS['SECRET']

ALLOWED_HOSTS = SETTINGS['ALLOWED_HOSTS']

# Application definition

INSTALLED_APPS = (
    'django.contrib.contenttypes',
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.postgres',
    # third-party
    'django_extensions',
    'post_office',
    'solo',
    # project
    'postal'
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'postal.urls'

CONTEXT_PROCESSORS = [
    'django.template.context_processors.debug',
    'django.template.context_processors.request',
    'django.contrib.auth.context_processors.auth',
    'django.contrib.messages.context_processors.messages'
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(SETTINGS['BASE_DIR'], 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages'
            ]
        },
    }
]

WSGI_APPLICATION = 'postal.wsgi.application'

# Data storages

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': SETTINGS['DB']['default']['NAME'],
        'USER': SETTINGS['DB']['default']['USER'],
        'PASSWORD': SETTINGS['DB']['default']['PASSWORD'],
        'HOST': SETTINGS['DB']['default']['HOST'],
        'PORT': SETTINGS['DB']['default']['PORT']
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "%s:%s:%s" % (SETTINGS['REDIS']['HOST'],
                                  SETTINGS['REDIS']['PORT'],
                                  SETTINGS['REDIS']['DB']['default']),
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

SESSION_ENGINE = 'redis_sessions.session'
SESSION_REDIS_HOST = SETTINGS['REDIS']['HOST']
SESSION_REDIS_PORT = SETTINGS['REDIS']['PORT']
SESSION_REDIS_DB = SETTINGS['REDIS']['DB']['sessions']
SESSION_REDIS_PREFIX = 'session'

# Internationalization

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(SETTINGS['BASE_DIR'], 'static')

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'compressor.finders.CompressorFinder',
)

# SORL

THUMBNAIL_KVSTORE = 'sorl.thumbnail.kvstores.redis_kvstore.KVStore'
THUMBNAIL_REDIS_DB = SETTINGS['REDIS']['DB']['thumbnails']
THUMBNAIL_PRESERVE_FORMAT = True

# Media files

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(SETTINGS['BASE_DIR'], 'media')

POSTCARD_CARD_DIR = 'postal/cards/'
POSTCARD_ORIGINAL_DIR = 'postal/original_images/'

FONT_FILE_DIR = 'postal/fonts/'

# Post Office

DEFAULT_FROM_EMAIL = 'no-reply@postal.com'

# Logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'INFO'),
        },
    },
}

# Auth

AUTH_USER_MODEL = 'postal.User'

# Postal settings

IMAGE_DIMENSIONS = {
    'height': {
        'min': 240,
        'max': 1968
    },
    'width': {
        'min': 320,
        'max': 2048
    }
}
