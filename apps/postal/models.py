from io import BytesIO
from PIL import Image, ImageFont, ImageDraw
from post_office import mail
from solo.models import SingletonModel
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.core.files.base import ContentFile
from django.core.files.uploadedfile import InMemoryUploadedFile
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils import timezone


class User(AbstractUser):
    pass


class Font(models.Model):
    title = models.CharField(verbose_name='Title', max_length=1000)
    file = models.FileField(verbose_name='File', upload_to=settings.FONT_FILE_DIR)

    class Meta:
        verbose_name = 'Font'
        verbose_name_plural = 'Fonts'
        ordering = ['title']

    def __str__(self):
        return self.title


class Postcard(models.Model):
    created_at = models.DateTimeField(verbose_name='Created at', default=timezone.now)
    author = models.ForeignKey('postal.User', verbose_name='Author', related_name='postcards')
    card = models.ImageField(verbose_name='Card', upload_to=settings.POSTCARD_CARD_DIR)
    original_image = models.ImageField(verbose_name='Original Image', upload_to=settings.POSTCARD_ORIGINAL_DIR)

    text = models.TextField(verbose_name='Text')
    font = models.ForeignKey('postal.Font', verbose_name='Font')
    font_size = models.PositiveSmallIntegerField(verbose_name='Font Size')
    pos_x = models.IntegerField(verbose_name='Text Position X')
    pos_y = models.IntegerField(verbose_name='Text Position Y')

    rgb_common = models.CharField(verbose_name='RGB most common', max_length=3)

    class Meta:
        verbose_name = 'Postcard'
        verbose_name_plural = 'Postcards'

    def send(self, sender, recipient, message):
        mail.send(
            recipients=recipient,
            sender=settings.DEFAULT_FROM_EMAIL,
            subject="Hey! It's' a postcard from {{sender}}!",
            message="{% if message %}{{message}}\n{% endif %}"
                    "{%if author %}Author of this postcard: {{author}}{% endif %}",
            context={
                'sender': sender.username,
                'author': self.author.username if self.author != sender else None,
                'message': message
            },
            attachments={
                self.card.file.name: self.card.path
            },
            priority='now'
        )

    @staticmethod
    def generate_card(instance):
        # Draw text on image using Pillow
        image = Image.open(instance.original_image.file)
        draw = ImageDraw.Draw(image)
        font = ImageFont.truetype(instance.font.file.path, instance.font_size)
        draw.text((instance.pos_x, instance.pos_y), instance.text, (0, 0, 0), font=font)

        # Saving image with byte stream
        buffer = BytesIO()
        image.save(fp=buffer, format=image.format)
        content_file = ContentFile(buffer.getvalue())
        image_file = InMemoryUploadedFile(content_file, 'card', instance.original_image.file.name,
                                          None, content_file.tell, None)
        return image_file

    @staticmethod
    def get_rgb_most_common(file):
        original_image = Image.open(file)
        converted_image = original_image.convert('RGB')  # Exclude alpha and possible troubles with GIF
        size = converted_image.size[0] * converted_image.size[1]

        # Finding most common color value
        result = [0, 0, 0]
        for color in converted_image.getcolors(size):
            for x in range(3):
                result[x] += color[0] * color[1][x]
        max_usage = max(result)

        # Checking for every component that is equal to common used value
        # e.g. white image will return 'rgb', while red will return 'r'
        most_common = []
        for index, color in enumerate(['r', 'g', 'b']):
            if result[index] == max_usage:
                most_common.append(color)
        return ''.join(most_common)


@receiver(pre_save, sender=Postcard)
def postcard_pre_save(sender, instance, *args, **kwargs):
    if instance.original_image and not instance.card:
        instance.card = Postcard.generate_card(instance)
    if instance.original_image and not instance.rgb_common:
        instance.rgb_common = Postcard.get_rgb_most_common(instance.original_image.file)


class SiteConfigModel(SingletonModel):
    postcard_text_max_length = models.PositiveIntegerField(verbose_name='Postcard text maximum length', default=0,
                                                           help_text='Set to 0 for unlimited maximum length')

    class Meta:
        verbose_name = "Site Configuration"
