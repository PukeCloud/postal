from io import BytesIO
from PIL import Image
from django import forms
from django.conf import settings
from django.contrib.auth.forms import AuthenticationForm
from django.core.files.images import get_image_dimensions
from django.core.exceptions import ValidationError
from .models import User, Postcard, SiteConfigModel


class RegisterForm(forms.ModelForm):
    password1 = forms.CharField(widget=forms.PasswordInput, label="Password")
    password2 = forms.CharField(widget=forms.PasswordInput, label="Verify password")

    class Meta:
        model = User
        fields = ['email', 'username']

    def clean(self):
        cleaned_data = super(RegisterForm, self).clean()
        if 'password1' in cleaned_data and 'password2' in cleaned_data \
                and cleaned_data['password1'] != cleaned_data['password2']:
            self.add_error('password2', "The two password fields didn't match.")

    def clean_email(self):
        return self.cleaned_data['email'].lower()


class LoginForm(AuthenticationForm):
    pass


class PostcardForm(forms.ModelForm):
    class Meta:
        model = Postcard
        fields = ['original_image', 'text', 'font', 'font_size', 'pos_x', 'pos_y']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['pos_x'].widget = forms.HiddenInput(attrs={'value': 0, 'class': 'posX'})
        self.fields['pos_y'].widget = forms.HiddenInput(attrs={'value': 0, 'class': 'posY'})
        self.fields['font'].empty_label = None
        self.fields['font_size'].initial = 50

    def clean_text(self):
        text = self.cleaned_data.get('text')
        config = SiteConfigModel.get_solo()
        if config.postcard_text_max_length and len(text) > config.postcard_text_max_length:
            raise ValidationError('Postcard text maximum length is %d' % config.postcard_text_max_length)
        return text

    def clean_original_image(self):
        image = self.cleaned_data.get('original_image')
        width, height = get_image_dimensions(image)
        limits = settings.IMAGE_DIMENSIONS
        if (limits['width']['min'] > width or limits['width']['max'] < width) and \
                (limits['height']['min'] > height or limits['height']['max'] < height):
            # Both dimensions are invalid
            raise ValidationError('Image dimensions are invalid')
        elif limits['width']['min'] < width < limits['width']['max'] and \
                                limits['height']['min'] < height < limits['height']['max']:
            # Both dimenstions are valid
            return image
        else:
            # Checking if able to resize with one invalid dimension
            if limits['width']['min'] < width < limits['width']['max']:
                new_height = max(limits['height']['min'], min(limits['height']['max'], height))
                new_width = round(new_height/height * width)
                if not limits['width']['min'] < new_width < limits['width']['max']:
                    raise ValidationError('Image dimensions are invalid')
            else:
                new_width = max(limits['width']['min'], min(limits['width']['max'], width))
                new_height = round(new_width/width * height)
                if not limits['height']['min'] < new_height < limits['height']['max']:
                    raise ValidationError('Image dimensions are invalid')

            # Resizing is valid. Now rescale the image
            image_file = BytesIO(image.read())
            new_image = Image.open(image_file)

            new_image = new_image.resize((new_width, new_height), Image.ANTIALIAS)

            new_image_file = BytesIO()
            try:
                new_image.save(new_image_file, image.name.split('.')[-1])
            except KeyError:
                new_image.save(new_image_file, 'JPEG')
            image.file = new_image_file
            return image


PostcardFormSet = forms.modelformset_factory(Postcard, PostcardForm)


class SendPostcardForm(forms.Form):
    recipient = forms.EmailField()
    message = forms.CharField(required=False, widget=forms.Textarea)
