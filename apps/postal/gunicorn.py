import multiprocessing

bind = "localhost:9000"
workers = multiprocessing.cpu_count() * 2 + 1
timeout = 600
errorlog = '-'