#!/bin/sh
## Packages
sudo apt-get update
sudo apt-get install $(cat settings/system_packages) --yes
sudo pip3 install -r settings/python_packages

## nginx
sudo cp settings/$1/postal /etc/nginx/sites-enabled/postal
sudo rm /etc/nginx/sites-enabled/default
if [ -f settings/$1/create_nginx_auth ]; then
    sudo expect settings/$1/create_nginx_auth
fi
sudo service nginx restart

## supervisor
sudo cp settings/$1/supervisor.conf /etc/supervisor/conf.d/supervisor.conf
sudo supervisorctl update
sudo supervisorctl restart all

## postgres
sudo sh settings/$1/postgres.sh