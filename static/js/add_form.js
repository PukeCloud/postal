$(window).on('load', function() {
    function addForm(template, selector, type) {
        var newElement = $(template).clone(true);
        var total = $('#id_' + type + '-TOTAL_FORMS').val();
        newElement.find(':input').each(function() {
            var name = $(this).attr('name').replace('__prefix__', total);
            var id = 'id_' + name;
            $(this).attr({'name': name, 'id': id});
        });
        newElement.find('label').each(function() {
            var newFor = $(this).attr('for').replace('__prefix__', total);
            $(this).attr('for', newFor);
        });
        total++;
        $('#id_' + type + '-TOTAL_FORMS').val(total);
        $(selector).after(newElement);
        newElement.show();
        newElement.find()
    }

    $('.add_form').on('click', function() {
        addForm($('#form_template'), $('.form:last'), 'form');
    });
});
